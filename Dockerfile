FROM python:3-alpine3.6

WORKDIR /app

COPY . /app

EXPOSE 8888

RUN pip install pipenv && pipenv install

CMD gunicorn -c gunicorn.py app.wsgi:application
