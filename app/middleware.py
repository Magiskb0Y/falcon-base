import json
import logging
import falcon


LOGGER = logging.getLogger("gunicorn.error")


class RequireJSON(object):
    def process_request(self, request, response):
        if not request.client_accepts_json:
            raise falcon.HTTPNotAcceptable(
                "This API only supports responses encoded as JSON.",
                href="https://www.json.org/")

        if request.method in ("POST", "PUT"):
            if "application/json" not in request.content_type:
                raise falcon.HTTPUnsupportedMediaType(
                    "This API only supports requests encoded as JSON.",
                    href="https://www.json.org/")


class JSONTranslator(object):
    def process_request(self, request, response):
        if request.content_length in (None, 0):
            return

        body = request.stream.read()
        if not body:
            raise falcon.HTTPBadRequest("Empty request body",
                                        "A valid JSON document is required.")

        try:
            data = json.loads(body.decode("utf-8"))
            setattr(req, "json", data)

        except (ValueError, UnicodeDecodeError):
            raise falcon.HTTPError(falcon.HTTP_753,
                                   "Malformed JSON",
                                   "Could not decode the request body. The "
                                   "JSON was incorrect or not encoded as "
                                   "UTF-8.")

    def process_response(self, request, response, resource, params):
        try:
            if not isinstance(response.json, dict):
                raise ValueError("Response must be a dict")
            response.body = json.dumps(response.json)
        except Exception as err:
            LOGGER.error(str(err))
            raise falcon.HTTPError(falcon.HTTP_500, "Server error")


class QueryStringParser(object):
    def process_request(self, request, response):
        if request.query_string is not str:
            request.query_string = ""
        query_strings = request.query_string.split("&")
        params = dict(tuple(map(lambda x: tuple(x.split("=")), query_strings)))
        request.params = params

    def process_response(self, request, response, resource, params):
        pass
