import falcon
from app import middleware
from app import resources


def create_app():
    application = falcon.API(
        middleware=[
            middleware.RequireJSON(),
            middleware.JSONTranslator(),
            middleware.QueryStringParser(),
        ]
    )

    application.add_route("/", resources.Demo())
    return application
